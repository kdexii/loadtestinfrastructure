# LoadTestInfrastructure



## Getting started

Requirements Linux version: Debian 11 bullseye

SSH (port: 22) access to all Work-Nodes from Master-node

You can create RSA private key for easy connection to hosts via ssh

![Diagram](image/Diagram.png)
