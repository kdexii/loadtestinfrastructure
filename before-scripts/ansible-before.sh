#!/bin/bash

ssh-keygen -t rsa -f certs/ansible-key

for ip in `cat list-Of-Servers.ini`; do
    ssh-copy-id -i certs/ansible-key.pub $ip
done
