#!/usr/bin/env bash
apt-get update
apt-get install vim sudo git -y

echo "$USER   ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USER